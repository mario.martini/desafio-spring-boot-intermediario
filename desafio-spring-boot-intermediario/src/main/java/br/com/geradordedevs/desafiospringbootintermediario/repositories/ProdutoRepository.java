package br.com.geradordedevs.desafiospringbootintermediario.repositories;

import br.com.geradordedevs.desafiospringbootintermediario.entities.ProdutoEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<ProdutoEntity, Long> {
}
