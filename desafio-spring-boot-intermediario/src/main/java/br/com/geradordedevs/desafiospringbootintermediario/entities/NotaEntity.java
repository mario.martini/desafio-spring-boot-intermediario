package br.com.geradordedevs.desafiospringbootintermediario.entities;

import javax.persistence.*;

@Entity
public class NotaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nomeAluno;
    private int primeiraNota;
    private int segundaNota;
    private int terceiraNota;
    private int quartaNota;
    private String status;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public int getPrimeiraNota() {
        return primeiraNota;
    }

    public void setPrimeiraNota(int primeiraNota) {
        this.primeiraNota = primeiraNota;
    }

    public int getSegundaNota() {
        return segundaNota;
    }

    public void setSegundaNota(int segundaNota) {
        this.segundaNota = segundaNota;
    }

    public int getTerceiraNota() {
        return terceiraNota;
    }

    public void setTerceiraNota(int terceiraNota) {
        this.terceiraNota = terceiraNota;
    }

    public int getQuartaNota() {
        return quartaNota;
    }

    public void setQuartaNota(int quartaNota) {
        this.quartaNota = quartaNota;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int media (){
        int media = (this.primeiraNota + this.segundaNota + this.terceiraNota + this.quartaNota) / 4;
        return media;
    }
}
