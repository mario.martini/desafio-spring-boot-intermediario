package br.com.geradordedevs.desafiospringbootintermediario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioSpringBootIntermediarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioSpringBootIntermediarioApplication.class, args);
	}
}
