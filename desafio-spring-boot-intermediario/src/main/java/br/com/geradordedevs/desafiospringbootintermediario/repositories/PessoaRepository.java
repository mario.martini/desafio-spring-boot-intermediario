package br.com.geradordedevs.desafiospringbootintermediario.repositories;

import br.com.geradordedevs.desafiospringbootintermediario.entities.PessoaEntity;
import org.springframework.data.repository.CrudRepository;

public interface PessoaRepository extends CrudRepository<PessoaEntity, Long> {
}
