package br.com.geradordedevs.desafiospringbootintermediario.controllers;

import br.com.geradordedevs.desafiospringbootintermediario.entities.NotaEntity;
import br.com.geradordedevs.desafiospringbootintermediario.entities.PessoaEntity;
import br.com.geradordedevs.desafiospringbootintermediario.repositories.NotaRepository;
import br.com.geradordedevs.desafiospringbootintermediario.repositories.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/notas")
public class NotaController {

    @Autowired
    private NotaRepository notaRepository;

    @GetMapping
    public Iterable<NotaEntity> listarNota (){
        return notaRepository.findAll();
    }

    @PostMapping
    public Iterable<NotaEntity> cadastrarNota (@RequestBody NotaEntity notaEntity){
        if (notaEntity.media() >= 7){
            notaEntity.setStatus("Aprovado");
        } else {
            notaEntity.setStatus("Reprovado");
        }
        notaRepository.save(notaEntity);
        return listarNota();
    }

    @GetMapping("/id/{id}")
    public Optional<NotaEntity> consultarNotaId (@PathVariable Long id){
        return notaRepository.findById(id);
    }

    @PutMapping("/{id}")
    public Iterable<NotaEntity> alterarNota (@PathVariable Long id, @RequestBody NotaEntity notaEntity){
        notaEntity.setId(id);
        notaRepository.save(notaEntity);
        return notaRepository.findAll();
    }
    @DeleteMapping("/{id}")
    public Iterable<NotaEntity> deletarNota (@PathVariable Long id){
        notaRepository.deleteById(id);
        return notaRepository.findAll();
    }

}
