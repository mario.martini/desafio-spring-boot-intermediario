package br.com.geradordedevs.desafiospringbootintermediario.controllers;

import br.com.geradordedevs.desafiospringbootintermediario.entities.NotaEntity;
import br.com.geradordedevs.desafiospringbootintermediario.entities.ProdutoEntity;
import br.com.geradordedevs.desafiospringbootintermediario.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoRepository produtoRepository;

    @GetMapping
    public Iterable<ProdutoEntity> listarProduto (){
        return produtoRepository.findAll();
    }

    @PostMapping
    public Iterable<ProdutoEntity> cadastrarProduto (@RequestBody ProdutoEntity produtoEntity){
        if (produtoEntity.getNomeProduto().length() > 20 && produtoEntity.getQuantidadeProduto() < 10){
            boolean disponivel = false;
            produtoEntity.setDisponibilidade(disponivel);
        }
        if (produtoEntity.getValorProduto() < 30 && produtoEntity.getValorProduto() > 450){
            boolean disponivel = false;
            produtoEntity.setDisponibilidade(disponivel);
        }
        if (produtoEntity.getTipoProduto() != "artesanato" || produtoEntity.getTipoProduto() != "automotivo"){
            boolean disponivel = false;
            produtoEntity.setDisponibilidade(disponivel);
        }
        if (produtoEntity.getTipoProduto() != "livros" || produtoEntity.getTipoProduto() != "celulares" ||  produtoEntity.getTipoProduto() != "informatica"){
            boolean disponivel = false;
            produtoEntity.setDisponibilidade(disponivel);
        }
        else {
            boolean disponivel = true;
            produtoEntity.setDisponibilidade(disponivel);
        }
        produtoRepository.save(produtoEntity);
        return listarProduto();
    }

    @GetMapping("/id/{id}")
    public Optional<ProdutoEntity> consultarProdutoId (@PathVariable Long id){
        return produtoRepository.findById(id);
    }

    @PutMapping("/{id}")
    public Iterable<ProdutoEntity> alterarProduto (@PathVariable Long id, @RequestBody ProdutoEntity produtoEntity){
        produtoEntity.setId(id);
        produtoRepository.save(produtoEntity);
        return produtoRepository.findAll();
    }
    @DeleteMapping("/{id}")
    public Iterable<ProdutoEntity> deletarProduto (@PathVariable Long id){
        produtoRepository.deleteById(id);
        return produtoRepository.findAll();
    }
}
