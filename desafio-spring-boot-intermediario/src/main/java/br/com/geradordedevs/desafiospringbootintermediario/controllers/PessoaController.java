package br.com.geradordedevs.desafiospringbootintermediario.controllers;

import br.com.geradordedevs.desafiospringbootintermediario.entities.PessoaEntity;
import br.com.geradordedevs.desafiospringbootintermediario.repositories.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    @Autowired
    private PessoaRepository pessoaRepository;

    @GetMapping
    public Iterable<PessoaEntity> listarPessoa(){
        return pessoaRepository.findAll();
    }

    @PostMapping
    public Iterable<PessoaEntity> cadastrarPessoa (@RequestBody PessoaEntity pessoaEntity){
        if (pessoaEntity.getIdade() >= 18){
            pessoaEntity.setMaioridade(true);
        } else {
            pessoaEntity.setMaioridade(false);
        }
        pessoaRepository.save(pessoaEntity);
        return listarPessoa();
    }

    @PutMapping("/{id}")
    public Iterable<PessoaEntity> alterarPessoa(@PathVariable Long id, @RequestBody PessoaEntity pessoaEntity){
        pessoaEntity.setId(id);
        pessoaRepository.save(pessoaEntity);
        return pessoaRepository.findAll();
    }

    @DeleteMapping("/{id}")
    public Iterable<PessoaEntity> remover(@PathVariable Long id){
        pessoaRepository.deleteById(id);
        return pessoaRepository.findAll();
    }

    @GetMapping("/id/{id}")
    public Optional<PessoaEntity> consultaPorId(@PathVariable Long id){
        return pessoaRepository.findById(id);
    }



}
