package br.com.geradordedevs.desafiospringbootintermediario.repositories;

import br.com.geradordedevs.desafiospringbootintermediario.entities.NotaEntity;
import org.springframework.data.repository.CrudRepository;

public interface NotaRepository extends CrudRepository<NotaEntity, Long> {
}
